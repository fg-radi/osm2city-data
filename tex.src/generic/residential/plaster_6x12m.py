from osm2city.textures.texture import Texture

for colour in ['red','orange','yellow','white','blue', 'aqua', 'green', 'cream', 'pink'] :

    facades.append(Texture('generic/residential/plaster_' + colour + '_6x12m.png',
        h_size_meters=6.0, h_cuts=[0, 256], h_can_repeat=True,
        v_size_meters=12.0, v_cuts=[0, 128, 256, 384, 512], v_can_repeat=False,
        v_align_bottom=True, height_min=0,
        requires=[],
        provides=['building:material:plaster','building:colour:'+colour, 'age:old', 'age:modern', 'shape:urban', 'compat:roof-flat', 'compat:roof-pitched']))
