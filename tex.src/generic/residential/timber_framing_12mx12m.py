from osm2city.textures.texture import Texture

for tb_color in ['red','yellow','white','blue'] :
    facades.append(Texture('generic/residential/timber_framing_' + tb_color + '_12x12m.png',
        h_size_meters=12.0, h_cuts=[0, 62, 193, 254, 317, 449,  512], h_can_repeat=True,
        v_size_meters=12.0, v_cuts=[0, 128, 256, 384, 512], v_can_repeat=False,
        v_align_bottom=True, height_min=0,
        requires=[],
        provides=['building:material:timber_framing','building:colour:'+tb_color, 'age:old', 'shape:urban', 'compat:roof-pitched']))
        
# ~ pink        
facades.append(Texture('generic/residential/timber_framing_pink_12x12m.png',
        h_size_meters=12.0, h_cuts=[0, 62, 193, 254, 317, 449,  512], h_can_repeat=True,
        v_size_meters=12.0, v_cuts=[0, 128, 256, 384, 512], v_can_repeat=False,        
        v_align_bottom=True, height_min=0,
        requires=[],
        provides=['building:material:timber_framing','building:colour:pink', 'building:colour:purple', 'building:colour:fushia', 'age:old', 'shape:urban', 'compat:roof-pitched']))

# ~ cream
facades.append(Texture('generic/residential/timber_framing_cream_12x12m.png',
        h_size_meters=12.0, h_cuts=[0, 62, 193, 254, 317, 449,  512], h_can_repeat=True,
        v_size_meters=12.0, v_cuts=[0, 128, 256, 384, 512], v_can_repeat=False,
        v_align_bottom=True, height_min=0,
        requires=[],
        provides=['building:material:timber_framing','building:colour:cream', 'building:colour:antiquewhite', 'age:old', 'shape:urban', 'compat:roof-pitched']))
