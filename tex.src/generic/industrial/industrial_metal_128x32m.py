from osm2city.textures.texture import Texture

for tb_color in ['red','orange','yellow','white','blue','grey','silver','green'  ] :

    facades.append(Texture('generic/industrial/industrial_metal_' + tb_color + '_128x32m.png',
        h_size_meters=128.0, h_cuts=[i for i in range(0, 230,10)] + [i for i in range(362, 750,10)] + [i for i in range(795, 1025,10)], h_can_repeat=True,
        v_size_meters=32.0, v_cuts=[0] + [i for i in range(64,257)], v_can_repeat=False,
        v_align_bottom=True, height_min=0,
        requires=[],
        provides=['specific', 'building:colour:'+tb_color, 'shape:industrial', 'shape:commercial', 'age:modern', 'compat:roof-flat','compat:roof-pitched']))
