
from osm2city.textures.texture import Texture

facades.append(Texture('de/residential/red_6x12m.png',
    h_size_meters=12.0, h_cuts=[658, 1333, 2052], h_can_repeat=True,
    v_size_meters=6, v_cuts=[403, 954], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['shape:urban', 'shape:residential', 'age:old', 'age:modern', 'compat:roof-pitched', 'compat:roof-flat']))
