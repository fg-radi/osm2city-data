
from osm2city.textures.texture import Texture


facades.append(Texture('de/residential/wbs70_36x36m.png',
    h_size_meters=33.6, h_cuts=[259, 520, 782, 1049, 1300, 1575], h_can_repeat=True,
    v_size_meters=33.2, v_cuts=[102, 219, 347, 466, 591, 708, 832, 951, 1085, 1210, 1323, 1464], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['shape:urban', 'shape:residential', 'age:modern', 'compat:roof-flat']))
