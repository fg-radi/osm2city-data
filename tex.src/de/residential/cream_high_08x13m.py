
from osm2city.textures.texture import Texture

facades.append(Texture('de/residential/cream_high_08x13m.png',
    h_size_meters=13.0, h_cuts=[1016, 1891, 1944, 2033, 2119, 2200, 2358], h_can_repeat=True,
    v_size_meters=8.0, v_cuts=[128, 880, 1632], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['shape:urban', 'shape:residential', 'age:old', 'compat:roof-pitched']))
