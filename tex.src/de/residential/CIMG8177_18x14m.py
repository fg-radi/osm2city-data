
from osm2city.textures.texture import Texture

facades.append(Texture('de/residential/CIMG8177_18x14m.png',
    h_size_meters=17.7, h_cuts=[560, 1362, 1876, 2446], h_can_repeat=True,
    v_size_meters=13.7, v_cuts=[1568, 2178], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['shape:urban', 'shape:residential', 'age:old', 'age:modern', 'compat:roof-pitched', 'compat:roof-flat']))
