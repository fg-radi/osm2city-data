
from osm2city.textures.texture import Texture

facades.append(Texture('de/industrial/transformer_07x03m.png',
    h_size_meters=7.5, h_cuts=[180, 745, 1540, 2322], h_can_repeat=True,
    v_size_meters=3.3, v_cuts=[960], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['shape:urban', 'power:substation', 'shape:industrial', 'age:old', 'age:modern', 'compat:roof-flat']))
