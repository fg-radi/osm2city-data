# -*- coding: utf-8 -*-
from osm2city.textures.texture import Texture


facades.append(Texture('stone.png',
    h_size_meters=7.40*4, h_cuts=[i/2. for i in range(0, 256*2)] , h_can_repeat=True,
    v_size_meters=60*4, v_cuts=[i/2. for i in range(0, 2024*2)], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    provides=[ 'specific', 'building:material:stone',  'building:colour:white', 'building:colour:yellow', 'age:old', 'compat:roof-flat','compat:roof-pitched']))

facades.append(Texture('stone_red.png',
    h_size_meters=7.40*4, h_cuts=[i/2. for i in range(0, 256*2)] , h_can_repeat=True,
    v_size_meters=60*4, v_cuts=[i/2. for i in range(0, 2024*2)], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    provides=[ 'specific', 'building:material:stone', 'building:colour:red', 'building:colour:red', 'age:old', 'compat:roof-flat','compat:roof-pitched']))
