
from osm2city.textures.texture import Texture

facades.append(Texture('gb/residential/sandstone1_13x06m.png',
    h_size_meters=13.4, h_cuts=[272, 574, 2040, 2815, 3060], h_can_repeat=True,
    v_size_meters=5.8, v_cuts=[615, 1542], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['shape:urban', 'shape:residential', 'age:old', 'age:modern', 'compat:roof-pitched', 'compat:roof-flat']))
