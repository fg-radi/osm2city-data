
from osm2city.textures.texture import Texture

facades.append(Texture('gb/residential/plaster3_10x03m.png',
    h_size_meters=9.9, h_cuts=[290, 1333, 2254, 3280, 3518, 3636], h_can_repeat=True,
    v_size_meters=3.3, v_cuts=[1164], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['shape:urban', 'shape:residential', 'age:old', 'age:modern', 'compat:roof-pitched', 'compat:roof-flat']))
