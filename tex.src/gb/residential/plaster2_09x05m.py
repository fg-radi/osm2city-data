
from osm2city.textures.texture import Texture

facades.append(Texture('gb/residential/plaster2_09x05m.png',
    h_size_meters=9.4, h_cuts=[295, 1540, 1901, 2562, 3396, 3696], h_can_repeat=True,
    v_size_meters=4.9, v_cuts=[887, 2246], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['shape:urban', 'shape:residential', 'age:old', 'age:modern', 'compat:roof-pitched', 'compat:roof-flat']))
