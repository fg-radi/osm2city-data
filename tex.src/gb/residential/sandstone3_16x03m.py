from osm2city.textures.texture import Texture

facades.append(Texture('gb/residential/sandstone3_16x03m.png',
    h_size_meters=15.5, h_cuts=[691, 1122, 1644, 2120, 2605, 2787, 2957, 3143, 3491, 3620, 3728], h_can_repeat=True,
    v_size_meters=2.6, v_cuts=[754], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['shape:urban', 'shape:residential', 'age:old', 'age:modern', 'compat:roof-pitched', 'compat:roof-flat']))
