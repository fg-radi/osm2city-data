# -*- coding: utf-8 -*-
from osm2city.textures.texture import Texture


facades.append(Texture('brick.png',
    h_size_meters=7.40, h_cuts=[i for i in range(0, 256)] , h_can_repeat=True,
    v_size_meters=60, v_cuts=[i for i in range(0, 2024)], v_can_repeat=False,
    v_align_bottom=True, height_min=0,
    requires=[],
    provides=['specific', 'building:colour:red', 'building:material:brick', 'age:old', 'compat:roof-flat','compat:roof-pitched']))
