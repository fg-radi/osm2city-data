AREA = 'HOLLAND'

USE_WS30_SCENERY = False
PREFIX = "WS30EHLE" if USE_WS30_SCENERY else "WS20EHLE"
PATH_TO_OUTPUT = "/home/vanosten/custom_scenery/" + PREFIX
PATH_TO_SCENERY = "/home/vanosten/custom_scenery/WS30_FSWeekend" if USE_WS30_SCENERY else "/home/vanosten/.fgfs/TerraSync"
PATH_TO_OSM2CITY_DATA = "/home/vanosten/develop_vcs/osm2city-data"

FG_ELEV = '/home/vanosten/bin/flightgear/dnc-managed/install/flightgear/bin/fgelev'

DB_HOST = "localhost"
DB_PORT = 5432
DB_NAME = "holland"
DB_USER = "gisuser"
DB_USER_PASSWORD = "Allegra1"


if AREA == 'HOLLAND':
    DB_NAME = "holland"  # covers all of the Netherlands

    FLAG_AFTER_2020_3 = True
    FLAG_STG_LOD_RADIUS = True

    C2P_PROCESS_TREES = True

    OWBB_LANDUSE_CACHE = True

    FG_ELEV_CACHE = False

    if not USE_WS30_SCENERY:
        OWBB_USE_BTG_LANDUSE = True


elif AREA == 'SWISS':
    pass

# Example command
# /home/vanosten/bin/virtualenvs/o2c310/bin/python3 /home/vanosten/develop_vcs/osm2city/build_tiles.py -f params.py -b *5.25_52.5_5.5_52.625 -o -p 1 -l DEBUG
